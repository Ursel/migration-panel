<?php
/**
 * Copyright 2010 - 2015, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2015, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace Migrationpanel\View\Helper;

use Cake\View\Helper;

/**
 * User helper
 */
class MigrationHelper extends Helper
{

    public $helpers = ['Html', 'Form'];

    /**
     * Social login link
     *
     * @param string $name name
     * @param array $options options
     * @return string
     */
    public function upDowngradeUrl(array $migration, $plugin)
    {
        if ($migration['status'] == 'up') {
            return $this->downgradeUrl($migration, $plugin);
        }
        return $this->upgradeUrl($migration, $plugin);
    }

    /**
     * @param array $migration
     * @param $plugin
     * @return string
     */
    private function upgradeUrl(array $migration, $plugin)
    {
        $url = "/migrationpanel/index/up/{$migration['id']}";
        if ($plugin !== 'App') {
            $url .= '/' . str_replace('/', '_', $plugin);
        }
        return $this->Html->link(
            'Upgrade',
            $url,
            []
        );
    }

    /**
     * @param array $migration
     * @param $plugin
     * @return string
     */
    private function downgradeUrl(array $migration, $plugin)
    {
        $url = "/migrationpanel/index/down/{$migration['previousTarget']}";
        if ($plugin !== 'App') {
            $url .= '/' . str_replace('/', '_', $plugin);
        }
        return $this->Html->link(
            'Downgrade',
            $url,
            []
        );
    }
}
